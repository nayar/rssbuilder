package main

import (
	"net/http"
	"strconv"

	"github.com/ant0ine/go-json-rest/rest"
	rss "github.com/KonishchevDmitry/go-rss"
)

type FeedsRest struct {
	feedsCol *FeedsCollection
	config *Config
}

func NewFeedsRest(feedsCol *FeedsCollection, config *Config) (*FeedsRest, error) {
	return &FeedsRest{feedsCol, config}, nil
}

func (feedsRest *FeedsRest) ListenAndServe(addr string) error {
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	api.Use(&rest.CorsMiddleware{
		RejectNonCorsRequests: false,
		OriginValidator: func(origin string, request *rest.Request) bool {
			return true
		},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
		AllowedHeaders: []string{
			"Accept", "Content-Type", "X-Custom-Header", "Origin", "Authorization"},
		AccessControlAllowCredentials: true,
		AccessControlMaxAge: 3600,
	})
	api.Use(&rest.AuthBasicMiddleware{
		Realm: "test zone",
		Authenticator: func(userId string, password string) bool {
			if userId == feedsRest.config.Username && password == feedsRest.config.Password {
				return true
			}
			return false
		},
	})

	router, err := rest.MakeRouter(
		rest.Get("/feeds", feedsRest.GetAllFeeds),
		rest.Get("/feeds/", feedsRest.GetAllFeeds),
		rest.Post("/feeds", feedsRest.CreateFeed),
		rest.Post("/feeds/", feedsRest.CreateFeed),
		rest.Get("/feeds/:feedId", feedsRest.GetFeed),
		rest.Put("/feeds/:feedId", feedsRest.UpdateFeed),
		rest.Delete("/feeds/:feedId", feedsRest.DeleteFeed),

		rest.Get("/feeds/:feedId/items", feedsRest.GetItems),
		rest.Get("/feeds/:feedId/items/", feedsRest.GetItems),
		rest.Post("/feeds/:feedId/items", feedsRest.CreateItem),
		rest.Post("/feeds/:feedId/items/", feedsRest.CreateItem),
		rest.Put("/feeds/:feedId/items/:itemId", feedsRest.UpdateItem),
		rest.Delete("/feeds/:feedId/items/:itemId", feedsRest.DeleteItem),
	)
	if err != nil {
		return err
	}
	api.SetApp(router)
	return http.ListenAndServe(addr, api.MakeHandler())
}

func (feedsRest *FeedsRest) GetAllFeeds(response rest.ResponseWriter, request *rest.Request) {
	response.WriteJson(feedsRest.feedsCol.feeds)
}

func (feedsRest *FeedsRest) GetFeed(response rest.ResponseWriter, request *rest.Request) {
	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	feed, ok := feedsRest.feedsCol.GetFeed(feedId)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	response.WriteJson(feed)
}

func (feedsRest *FeedsRest) CreateFeed(response rest.ResponseWriter, request *rest.Request) {
	var feed rss.Feed
	err := request.DecodeJsonPayload(&feed)
	if err != nil {
		rest.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	id := feedsRest.feedsCol.AddFeed(&feed)
	feedsRest.feedsCol.Save()
	response.WriteJson(id)
}

func (feedsRest *FeedsRest) UpdateFeed(response rest.ResponseWriter, request *rest.Request) {
	var feed rss.Feed
	err := request.DecodeJsonPayload(&feed)
	if err != nil {
		rest.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	ok := feedsRest.feedsCol.UpdateFeed(feedId, &feed)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	feedsRest.feedsCol.Save()
	response.WriteHeader(http.StatusOK)
}

func (feedsRest *FeedsRest) DeleteFeed(response rest.ResponseWriter, request *rest.Request) {
	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	ok := feedsRest.feedsCol.DeleteFeed(feedId)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	feedsRest.feedsCol.Save()
	response.WriteHeader(http.StatusOK)
}

func (feedsRest *FeedsRest) GetItems(response rest.ResponseWriter, request *rest.Request) {
	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	feed, ok := feedsRest.feedsCol.GetFeed(feedId)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	items := feed.Items
	if items == nil {
		items = []*rss.Item{}
	}
	response.WriteJson(items)
}

func (feedsRest *FeedsRest) CreateItem(response rest.ResponseWriter, request *rest.Request) {
	var item rss.Item
	err := request.DecodeJsonPayload(&item)
	if err != nil {
		rest.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	feedsRest.feedsCol.AddItem(feedId, &item)
	feedsRest.feedsCol.Save()
	response.WriteJson(item)
}

func (feedsRest *FeedsRest) UpdateItem(response rest.ResponseWriter, request *rest.Request) {
	var item rss.Item
	err := request.DecodeJsonPayload(&item)
	if err != nil {
		rest.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}
	itemId, err := strconv.Atoi(request.PathParam("itemId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	ok := feedsRest.feedsCol.UpdateItem(feedId, itemId, &item)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	feedsRest.feedsCol.Save()
	response.WriteHeader(http.StatusOK)
}

func (feedsRest *FeedsRest) DeleteItem(response rest.ResponseWriter, request *rest.Request) {
	feedId, err := strconv.Atoi(request.PathParam("feedId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}
	itemId, err := strconv.Atoi(request.PathParam("itemId"))
	if err != nil {
		rest.NotFound(response, request)
		return
	}

	ok := feedsRest.feedsCol.DeleteItem(feedId, itemId)
	if !ok {
		rest.NotFound(response, request)
		return
	}

	feedsRest.feedsCol.Save()
	response.WriteHeader(http.StatusOK)
}
