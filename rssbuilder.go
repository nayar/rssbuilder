package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
)

type Config struct {
	Username string
	Password string
}

func main() {
	var rssFile = flag.String("f", "/tmp/rssbuilder.rss", "RSS File")
	var httpPort = flag.String("p", "8050", "HTTP Port")
	var confFile = flag.String("c", "config.json", "Config File")
	flag.Parse()

	log.Println("Starting Service")

	config, err := readConf(*confFile)
	if err != nil {
		log.Fatalln(err)
	}

	feedsCol, err := NewFeedsCollection(*rssFile)
	if err != nil {
		log.Fatalln(err)
	}

	feedsRest, err := NewFeedsRest(feedsCol, config)
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("File: %s, port: %s\n", *rssFile, *httpPort)
	if err := feedsRest.ListenAndServe("0.0.0.0:" + *httpPort); err != nil {
		log.Fatalln(err)
	}
}

func readConf(confFile string) (*Config, error) {
	file, _ := os.Open(confFile)
	defer file.Close()

	var config Config
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&config)

	return &config, err
}
