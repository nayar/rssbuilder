package main

import (
	"io/ioutil"
	"strconv"

	rss "github.com/KonishchevDmitry/go-rss"
)

type FeedsCollection struct {
	rssFile string
	feeds []*rss.Feed
}

func NewFeedsCollection(rssFile string) (*FeedsCollection, error) {
	feedsCol := &FeedsCollection{rssFile, []*rss.Feed{}}

	rssData, err := ioutil.ReadFile(rssFile)
	if err == nil {
		feed, err := rss.Parse(rssData)
		if err != nil {
			return nil, err
		}
		feedsCol.feeds = append(feedsCol.feeds, feed)
	}

	return feedsCol, nil
}

func (feedsCol *FeedsCollection) Save() error {
	data, err := rss.Generate(feedsCol.feeds[0])
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(feedsCol.rssFile, data, 0644)
	return err
}

func (feedsCol *FeedsCollection) AddFeed(feed *rss.Feed) bool {
	feedsCol.feeds = append(feedsCol.feeds, feed)
	return true
}

func (feedsCol *FeedsCollection) GetFeed(feedId int) (*rss.Feed, bool) {
	if len(feedsCol.feeds) < feedId || feedId < 1 {
		return nil, false
	}
	return feedsCol.feeds[feedId - 1], true
}

func (feedsCol *FeedsCollection) UpdateFeed(feedId int, newFeed *rss.Feed) bool {
	feed, ok := feedsCol.GetFeed(feedId)
	if !ok {
		return false
	}

	feed.Title = newFeed.Title
	feed.Description = newFeed.Description
	return true
}

func (feedsCol *FeedsCollection) DeleteFeed(feedId int) bool {
	if len(feedsCol.feeds) < feedId || feedId < 1 {
		return false
	}

	feedsCol.feeds = append(feedsCol.feeds[:feedId - 1], feedsCol.feeds[feedId:]...)
	return true
}

func (feedsCol *FeedsCollection) AddItem(feedId int, item *rss.Item) bool {
	feed, ok := feedsCol.GetFeed(feedId)
	if !ok {
		return false
	}

	// Calculate new id
	maxId := 0
	for i := 0; i < len(feed.Items); i++ {
		if itemId, _ := strconv.Atoi(feed.Items[i].Guid.Id); itemId > maxId {
			maxId = itemId
		}
	}

	item.Guid.Id = strconv.Itoa(maxId + 1)
	feed.Items = append(feed.Items, item)
	return true
}

func (feedsCol *FeedsCollection) GetItemIndex(feedId int, itemId int) (*rss.Feed, int) {
	feed, ok := feedsCol.GetFeed(feedId)
	if !ok {
		return nil, -1
	}

	strId := strconv.Itoa(itemId)
	for i := 0; i < len(feed.Items); i++ {
		if feed.Items[i].Guid.Id == strId {
			return feed, i
		}
	}

	return nil, -1
}

func (feedsCol *FeedsCollection) UpdateItem(feedId int, itemId int, item *rss.Item) bool {
	feed, itemIndex := feedsCol.GetItemIndex(feedId, itemId)
	if itemIndex == -1 {
		return false
	}

	item.Guid.Id = feed.Items[itemIndex].Guid.Id
	feed.Items[itemIndex] = item
	return true
}

func (feedsCol *FeedsCollection) DeleteItem(feedId int, itemId int) bool {
	feed, itemIndex := feedsCol.GetItemIndex(feedId, itemId)
	if itemIndex == -1 {
		return false
	}

	feed.Items = append(feed.Items[:itemIndex], feed.Items[itemIndex + 1:]...)
	return true
}
